$(function() {

  Parse.$ = jQuery;

  Parse.initialize("c5i4G4MbJ2odnn2OBh2a7tolKhldSHBDg7DbK2eO",
                   "RgwrHfPKjxb3v9FQYux4GeizFiBbkBDcASdbiSF9");

  // Todo Model
  // ----------


  var durum = 0;

  // Our basic Todo model has `content`, `order`, and `done` attributes.
  var Todo = Parse.Object.extend("Todo", {
    // Default attributes for the todo.
    defaults: {
      content: "empty todo...",
      done: false,
    },

    // Ensure that each todo created has `content`.
    initialize: function() {
      if (!this.get("content")) {
        this.set({"content": this.defaults.content});
      }
    },

    // Toggle the `done` state of this todo item.
    toggle: function() {
      this.save({done: !this.get("done")});
    }
  });

  var Log = Parse.Object.extend("GunlukEntity", {
    // Default attributes for the todo.
    defaults: {
     // content: "empty todo...",
      done: false,
      icerik: "empty",
      baslik: "empty",
      date: new Date(),
      favorimi: false,
      isDraft: false,
      objectId:"",
      uuid:""
    },

    // Ensure that each todo created has `content`.
    initialize: function() {

      if (!this.get("icerik")) {
        this.set({"icerik": this.defaults.icerik});
      }
      if (!this.get("baslik")) {
        this.set({"baslik": this.defaults.baslik});
      }
      if (!this.get("date")) {
        this.set({"date": this.defaults.date});
      }
    },

    // Toggle the `done` state of this todo item.
    toggle: function() {
      this.save({done: !this.get("done")});

    }
  });

  // This is the transient application state, not persisted on Parse
  var AppState = Parse.Object.extend("AppState", {
    defaults: {
      filter: "all"
    }
  });

  function openModal(){

  }

  // Todo Collection
  // ---------------

  var id= "";

  var LogList = Parse.Collection.extend({

    // Reference to this collection's model.
    model: Log,

    // Filter down the list of all todo items that are finished.
    done: function() {
      return this.filter(function(log){ return log.get('favorimi'); });
    },

    // Filter down the list to only todo items that are still not finished.
    remaining: function() {
      return this.without.apply(this, this.done());
    },

    // We keep the Todos in sequential order, despite being saved by unordered
    // GUID in the database. This generates the next order number for new items.
    nextOrder: function() {
      if (!this.length) return 1;
      return this.last().get('order') + 1;
    },

    // Todos are sorted by their original insertion order.
    comparator: function(todo) {
      return todo.get('date');
    }

  });

  var LogView = Parse.View.extend({

    //... is a list tag.
    tagName:  "li",

    // Cache the template function for a single item.
    template: _.template($('#item-template').html()),

    // The DOM events specific to an item.
    events: {
      "click .toggle"              : "toggleDone",
      "click .todo-destroy"   : "open",
      "blur .edit"          : "close",
      "click #btnSil" : "deleteEntry",
      "click #btnEdit" : "editModalOpen",
      "click #btnDegistir" : "editSave",
      "click #btnFav" : "fav",
      "click #btnUnFav":"unFav"
    },

    // The TodoView listens for changes to its model, re-rendering. Since there's
    // a one-to-one correspondence between a Todo and a TodoView in this
    // app, we set a direct reference on the model for convenience.
    initialize: function() {
      _.bindAll(this, 'render', 'close', 'remove');
      this.model.bind('change', this.render);
      this.model.bind('destroy', this.remove);
    },

    // Re-render the contents of the todo item.
    render: function() {
      $(this.el).html(this.template(this.model.toJSON()));

      this.input = this.$('.edit');
      return this;
    console.log("d");
    },

    // Toggle the `"done"` state of the model.
    toggleDone: function() {
      this.model.toggle();
    },

    // Switch this view into `"editing"` mode, displaying the input field.
    editModalOpen: function() {
      var baslikId = "baslikLabel"+this.model.id;
      var icerikId = "icerikLabel"+this.model.id;
      //$('#editModalId').attr('id',this.model.id)
      $('#baslikLabel').attr('id',baslikId);
      $('#icerikLabel').attr('id',icerikId);
      $('input[id="editModalId"]').val(this.model.id);
      $(this.el).addClass("editing");
      $('input[id="inputBaslikEdit"]').val(this.model.baslik);
      $('input[id="inputBaslikEdit"]').val($('#'+baslikId).text());
      $('textarea[id="textAreaIcerikEdit"]').val($('#'+icerikId).text());
      $('#editModal').modal({keyboard:true});
    },

    editSave : function() {
      var id = $('input[id="editModalId"]').val();
      var Edit = Parse.Object.extend("GunlukEntity");
      var query = new Parse.Query(Edit);
      query.equalTo("objectId", id);
      query.find({
        success: function(results) {
          for (var i = 0; i < results.length; i++) {
            var object = results[i];
            this.model = object;
            console.log(object);
            console.log(this.model);
            this.model.save({
              baslik: $('input[id="inputBaslikEdit"]').val(),
              icerik: $('textarea[id="textAreaIcerikEdit"]').val()
              // $(this.el).removeClass("editing");
            });
          }
          var baslikId = "baslikLabel"+id;
          var icerikId = "icerikLabel"+id;

          $('#'+baslikId).text( $('input[id="inputBaslikEdit"]').val());
          $('#'+icerikId).text($('textarea[id="textAreaIcerikEdit"]').val());
          $('#toastEdit').snackbar("show");

        },
        error: function(error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });

      console.log("logs : " + this.logs);
      //this.logs.each(console.log(this));
    },
    // Close the `"editing"` mode, saving changes to the todo.
    close: function() {
      this.model.save({content: this.input.val()});
      $(this.el).removeClass("editing");
    },

    fav: function() {
      var favId = "btnFav"+this.model.id;
      this.model.save({
        favorimi: true
      });
      $('#'+favId).hide();
      $('#toastFav').snackbar("show");

    },

    unFav: function() {
      var unFavId = "btnUnFav"+this.model.id;
      this.model.save({
        favorimi: false
      });
      $('#'+unFavId).hide();

      $('#toastUnFav').snackbar("show");
    },

    clear: function() {
      this.model.destroy();
    },

    open: function()  {

    $('#yesNoModal').modal({keyboard:true});
    console.log(this.model);
    $('input[id="dmId"]').val(this.model.id);
    console.log(this.model.id);

  },

    deleteEntry: function() {

      var id = $('input[id="dmId"]').val();
      console.log(id);
      $('#123').attr('id',id);
      //$("ul.list-group").attr("id",id);
      var Delete = Parse.Object.extend("GunlukEntity");
      var query = new Parse.Query(Delete);
      query.equalTo("objectId", id);
      query.find({
        success: function(results) {
          for (var i = 0; i < results.length; i++) {
            var object = results[i];
           //this.model = object;
            console.log(object)
          }

          object.destroy();
          //this.model.destroy();
          $('#yesNoModal').modal('hide');
          $('#' + id).remove();
          $('#toastDelete').snackbar("show");
        },
        error: function(error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });

    }

  });

  // The main view that lets a user manage their todo items

  var ManageLogView = Parse.View.extend({

    // Our template for the line of statistics at the bottom of the app.
    statsTemplate: _.template($('#stats-template').html()),

    // Delegated events for creating new items, and clearing completed ones.
    events: {
      "click #clear-completed": "clearCompleted",
      "click .log-out": "logOut",
      "click ul#filters a": "selectFilter",
      "click  #btnKaydet ": "createOnSave",
      "keyup #inputSearch" : "searchLogs",
      "click ul#navbar a":"hearderSelectFilter"
    },

   el: ".content",

    // At initialization we bind to the relevant events on the `Todos`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting todos that might be saved to Parse.
    initialize: function() {
      var self = this;

      _.bindAll(this, 'addOne', 'addAll', 'addSome', 'render', 'logOut', 'createOnSave');

      // Main todo management template
      this.$el.html(_.template($("#manage-logs-template").html()));



      // Create our collection of Todos
      this.logs = new LogList;

      // Setup the query for the collection to look for todos from the current user
      this.logs.query = new Parse.Query(Log);
      this.logs.query.ascending('createdAt');
      this.logs.query.equalTo("author", Parse.User.current());
     // this.logs.query.endsWith("icerik","k");
      //this.logs.query.limit(8);
      //deneme amacıyla eklendi... !!!
     // this.logs.query.orderByDescending("createdAt");
     // query.descending("createdAt");

      this.logs.bind('add',     this.addOne);
      this.logs.bind('reset',   this.addAll);
      this.logs.bind('all',     this.render);

      // Fetch all the todo items for this user

      this.logs.fetch();


      state.on("change", this.filter, this);
    },

    searchLogs: function() {

      //this.$("#todo-list").html("");

      this.logs = new LogList;

      this.logs.bind('add',     this.addOne);
      this.logs.bind('reset',   this.addAll);
      this.logs.bind('all',     this.render);

      var aranan = $('#inputSearch').val();

      console.log(aranan);
      this.logs.query = new  Parse.Query(Log);
      //this.logs.query.equalTo("author", Parse.User.current());
      this.logs.query.contains("icerik",aranan);
      this.logs.query.ascending("createdAt");
      this.logs.fetch();
    },
    // Logs out the user and shows the login view
    logOut: function(e) {
      Parse.User.logOut();
      new LogInView();
      this.undelegateEvents();
      delete this;
    },

    // Re-rendering the App just means refreshing the statistics -- the rest
    // of the app doesn't change.
    render: function() {
      var done = this.logs.done().length;
      var remaining = this.logs.remaining().length;

      this.$('#todo-stats').html(this.statsTemplate({
        total:      this.logs.length,
        done:       done,
        remaining:  remaining
      }));

      this.delegateEvents();
    },

    // Filters the list based on which type of filter is selected
    hearderSelectFilter : function(e) {
      var el = $(e.target);
      var filterValue = el.attr("id");
      state.set({headerFilter : filterValue});
      Parse.history.navigate(filterValue);
      console.log("headerSelec girdi ....");

      if (filterValue === "headerAll") {


        this.logs = new LogList;

        this.logs.query = new Parse.Query(Log);
        this.logs.query.ascending('createdAt');
        this.logs.query.equalTo("author", Parse.User.current());

        this.logs.bind('add',     this.addOne);
        this.logs.bind('reset',   this.addAll);
        this.logs.bind('all',     this.render);

        this.logs.fetch();

        this.addAll();
      }
      else {this.addSome(function(item) { return item.get('favorimi') });}

    },

    selectFilter: function(e) {
      var el = $(e.target);
      var filterValue = el.attr("id");
      state.set({filter: filterValue});
      Parse.history.navigate(filterValue);
    },

    filter: function() {
      var filterValue = state.get("filter");
      this.$("ul#filters a").removeClass("selected");
      this.$("ul#filters a#" + filterValue).addClass("selected");
      if (filterValue === "all") {
        this.addAll();
      } else if (filterValue === "favorite") {
        this.addSome(function(item) { return item.get('favorimi') });

      }
      else {
        this.addSome(function(item) { return !item.get('favorimi') });
      }
    },

    // Resets the filters to display all todos
    resetFilters: function() {
      this.$("ul#filters a").removeClass("selected");
      this.$("ul#filters a#all").addClass("selected");
      this.addAll();
    },

    // Add a single todo item to the list by creating a view for it, and
    // appending its element to the `<ul>`.
    addOne: function(log) {
      var view = new LogView({model: log});
      this.$("#todo-list").append(view.render().el);
    },

    // Add all items in the Todos collection at once.
    addAll: function(collection, filter) {
      this.$("#todo-list").html("");
      this.logs.each(this.addOne);
    },

    // Only adds some todos, based on a filtering function that is passed in
    addSome: function(filter) {
      var self = this;
      this.$("#todo-list").html("");
      this.logs.chain().filter(filter).each(function(item) { self.addOne(item) });
    },

    createOnSave: function(e){

      console.log("Save started.");
      console.log("this.baslik.val:" +  $('input[id="inputBaslik"]').val());
      console.log("icerik.val: "+ $('textarea[id="textAreaIcerik"]').val());


      function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
      }

      var zaman = new Date();
      var saat = zaman.getHours();
      var uuid = guid();
      this.logs.create({
        // content: this.input.val()z

        done: false,
        order: this.logs.nextOrder(),
        author: Parse.User.current(),
        ACL: new Parse.ACL(Parse.User.current()),
        baslik: $('input[id="inputBaslik"]').val(),
        icerik: $('textarea[id="textAreaIcerik"]').val(),
        favorimi: false,
        date: zaman,
        uuid: uuid
      },
          {wait:'true'}
      );

      console.log(this.logs.id);
      $('input[id="inputBaslik"]').val('');
      $('textarea[id="textAreaIcerik"]').val('');


     // $('#myModal').modal({keyboard:false});
      console.log("zaman dilimi : "+zaman);
      $('#toastAdd').snackbar("show");

    },

    // Clear all done todo items, destroying their models.
    clearCompleted: function() {
      _.each(this.logs.done(), function(todo){ todo.destroy(); });
      return false;
    },

  });

  var LogInView = Parse.View.extend({
    events: {
      "submit form.login-form": "logIn",
      "submit form.signup-form": "signUp",
      "click #btnSifremiUnutum": "forget"
    },

    el: ".content",

    
    initialize: function() {
      _.bindAll(this, "logIn", "signUp","forget");
      this.render();
    },

    forget: function(e) {
     // var self = this;
      durum = 2;
      console.log("forget logInViewwwww")
      new ForgetPassView();
      //self.undelegateEvents();
      //delete self;
    },

    logIn: function(e) {
      var self = this;
      var username = this.$("#login-username").val();
      var password = this.$("#login-password").val();
      
      Parse.User.logIn(username, password, {
        success: function(user) {
          new ManageLogView();
          self.undelegateEvents();
          delete self;
        },

        error: function(user, error) {
          self.$(".login-form .error").html("Invalid username or password. Please try again.").show();
          self.$(".login-form button").removeAttr("disabled");
        }
      });

      this.$(".login-form button").attr("disabled", "disabled");

      return false;
    },

    signUp: function(e) {
      var self = this;
      var username = this.$("#signup-username").val();
      var password = this.$("#signup-password").val();
      var email = this.$("#signup-email").val();
      
      Parse.User.signUp(username, password,{ ACL: new Parse.ACL(),email:email }, {
        success: function(user) {//author olan yer user ile degişti... yada tam tersi...
          new ManageLogView();
          self.undelegateEvents();
          delete self;
        },

        error: function(user, error) {
          self.$(".signup-form .error").html(_.escape(error.message)).show();
          self.$(".signup-form button").removeAttr("disabled");
        }
      });

      this.$(".signup-form button").attr("disabled", "disabled");

      return false;
    },

    render: function() {
      this.$el.html(_.template($("#login-template").html()));
      this.delegateEvents();
    }
  });

  var ForgetPassView = Parse.View.extend({

    //template:_.template($('#forget-pass-template').html()),

    events:{
      "submit form.forget-form": "forget"
    },

    el:".content",

    initialize: function(){
      var self = this;
      _.bindAll(this,"forget");

      this.render();
    },

    render:function() {
      this.$el.html(_.template($("#forget-pass-template").html()));
      this.delegateEvents();
    },

    forget: function() {

      var resetEmail = $("#reset-email").val();
      Parse.User.requestPasswordReset(resetEmail , {
        success: function() {
          // Password reset request was sent successfully
          console.log("basariyla gonderildi");
         // new LogInView();
        },
        error: function(error) {
          // Show the error message somewhere
          alert("Error: " + error.code + " " + error.message);
        }
      });

    }
  });

  // The main view for the app
  var AppView = Parse.View.extend({
    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: $("#todoapp"),

    initialize: function() {
      this.render();
    },

    render: function() {
      if (Parse.User.current()) {
        new ManageLogView();
      }else  {
          if(durum === 0) {
            new LogInView();
          }
          else{
            new ForgetPassView();
            //LogInView();
          }
      }
    }
  });

  var AppRouter = Parse.Router.extend({
    routes: {
      "all": "all",
      "active": "active",
      "completed": "completed"
    },

    initialize: function(options) {
    },

    all: function() {
      state.set({ filter: "all" });
    },

    active: function() {
      state.set({ filter: "active" });
    },

    completed: function() {
      state.set({ filter: "completed" });
    }
  });

  var state = new AppState;

  new AppRouter;
  new AppView;
  Parse.history.start();
});


